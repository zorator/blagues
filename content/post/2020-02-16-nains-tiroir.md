---
title: Nains à tiroir
date: 2020-02-16
tags: ["nain", "tiroir"]
---

*Comment appelle-t-on un nain qui sort de la boulangerie avec une baguette ?*
> Un bloc-note, car c'est un petit qu'a le pain !

*Comment appelle-t-on un nain qui sort des toilettes ?*
> Un expresso, car c'est un ptit qu'a fait !

*Comment appelle-t-on un nain qui se promène avec une sono sur l'épaule ?*
> Un slip, car c'est un ptit qu'a le son !

*Comment appelle-t-on un nain qui possède une étandue d'eau ?*
> Un poulpe, car c'est un ptit qu'a la mare !

*Comment appelle-t-on un nain qui sait tout ?*
> Un chef, car c'est un nain compétent !

*Comment appelle-t-on un nain qui déteste tout le monde ?*
> Un stressé, car c'est un nain qui hait !

*Comment appelle-t-on un nain qui ne remercie personne ?*
> Un obèse, car c'est un nain gras !

*Comment appelle-t-on un nain qui se fait passer pour quelqu'un d'autre ?*
> Un facteur, car c'est un nain posteur !

*Dans quel lieu peut-on trouver des canards nains ?*
> Aux toilettes, car ce sont des petits-coins !
