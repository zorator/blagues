---
title: Legume marin
date: 2020-02-22
tags: ["legume", "sous-marin", "vite"]
---

*Qu'est-ce qui est vert et qui va sous l'eau ?*
> Un chou-marin !

*Qu'est-ce qui est vert, qui va sous l'eau et qui fait bzzzz ?*
> Un chou-marin ruche !

*Qu'est-ce qu'un chou qui va très très vite en rigolant ?*
> Un chou marreur
