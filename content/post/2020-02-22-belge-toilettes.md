---
title: Les Belges aux toilettes
date: 2020-02-22
tags: ["belge", "toilettes"]
---

*Pourquoi les Belges vont-ils aux toilettes avec du pain ?*
> C'est pour nourrir canard WC !

*Pourquoi les Belges vont-ils aux toilettes avec un fusil ?*
> C'est pour tirer la chasse !

*Pourquoi les Belges ne ferment-ils jamais la porte des WC ?*
> C'est pour qu'on ne puisse pas regarder par le trou de la serrure !
