---
title: Char français
date: 2020-04-01
tags: ["armée", "france"]
---

*Pourquoi les chars français sont-ils équipés d'une seule vitesse pour aller en avant, et de 5 vitesses pour la marche arrière ?*
> Parce qu'il y a une vitesse pour les défilés, et 5 pour la retraite ! !
