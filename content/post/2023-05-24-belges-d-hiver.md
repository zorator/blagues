---
title: Nos amis les Belges
date: 2023-05-24
tags: ["belge", "moto", "pyjama", "liquide", "ave", "maria", "contrepèterie"]
author: Nicolas C.
---

*Pourquoi les Belges font-ils de la moto en pyjama ?*
> Pour pouvoir se coucher dans les virages !

*Pourquoi les Belges vont-ils faire les courses avec des seaux d'eau ?*
> Pour payer en liquide !

*Pourquoi les Belges vont-ils à la messe avec des seaux d'eau ?*
> Pour l'Avé Maria !

*Quelle est la contrepèterie belge qui a le plus de succès ?*
> Il fait beau et chaud.
