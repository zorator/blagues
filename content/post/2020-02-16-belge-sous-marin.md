---
title: Sous-marin belge
date: 2020-02-16
tags:
- belge
- sous-marin

---
_Tous les ans, le même jour, au même moment, tous les sous-marins belges coulent._  
_Pourquoi ?_

> Parce que c'est journée portes ouvertes !