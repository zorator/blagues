---
title: La loi des gaz imparfaits
date: 2020-02-29
tags: ["gaz", "chimie"]
author: Clément M.
---

*Quelle est la loi des gaz parfaits ?*
> \\( PV = nRT \\)

*Quelle est la loi des gaz **imparfaits** ?*
> \\( PT = nRV \\) !
