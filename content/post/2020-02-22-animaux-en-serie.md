---
title: Animaux en série
date: 2020-02-22
tags: ["animaux", "frigo", "girafe", "éléphant", "lion", "crocodile", "anniversaire", "série-de-blagues"]
---

*Comment met-on un éléphant dans un frigo ?*
> On ouvre la porte, on met l'éléphant et on ferme la porte.

*Comment met-on une girafe dans un frigo ?*
> On ouvre la porte, on retire l'éléphant, on met la girafe et on ferme la porte.

*Le roi lion organise une grande fête pour son anniversaire, avec tous les animaux.
Tous viennent sauf un, lequel ?*
> La girafe, elle est dans le frigo !

*Indiana Jones explore un temple pour récupérer une statuette en or pur.
Pour y parvenir, il doit traverser une rivière décrite par un panneau comme infestée de crocodiles.
Il traverse quand même, et arrive de l'autre côté sans aucun soucis, pourquoi ?*
> Les crocodiles sont à l'anniversaire du lion !