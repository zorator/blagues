# ViviHumour

## Disclaimer

Ce site est dédié à l'humour (no way).
Si un humour en particulier ne vous convient pas, ne lisez pas :)


## Proposer une blague

Il suffit d'ouvrir une merge request en ajoutant un fichier dans `content/post` en respectant le format suivant : 
`yyyy-MM-dd-titre-short.md`

Template de fichier : 

```md
---
title: Titre
date: yyyy-MM-dd
tags: ["tag1", ...]
author: Prénom N.
---

*Texte*
> Réponse
```